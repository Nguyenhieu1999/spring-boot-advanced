package com.java.spring.entity;
 

import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
 
@Entity
@Table(name = "Accounts", uniqueConstraints = {@UniqueConstraint(columnNames = "User_name")})
@AttributeOverride(name = "User_Name", column = @Column(name ="User_Name", nullable = false, columnDefinition = "VARCHAR UNSIGNED"))
public class Account extends BaseEntity{

	private static final long serialVersionUID = -2054386655979281969L;
 
//    public static final String ROLE_MANAGER = "MANAGER";
//    public static final String ROLE_EMPLOYEE = "EMPLOYEE";
// 
    @Column(name = "User_Name", length = 20, nullable = false)
    private String userName;
 
    @Column(name = "Encryted_Password", length = 128, nullable = false)
    private String encrytedPassword;
 
    @Column(name = "Active", length = 1, nullable = false)
    private boolean active;
 
    @Column(name = "User_Role", length = 20, nullable = false)
    private String userRole;
    public Account() {
    	super();
    }

    public Account(int id, Date createdAt, Date updatedAt, Date deletedAt, boolean isDeleted, String userName, String encrytedPassword, boolean active, String userRole) {
    	super(id, createdAt, updatedAt, deletedAt, isDeleted);
    	this.userName = userName;
    	this.active = active;
    	this.encrytedPassword = encrytedPassword;
    	this.userRole = userRole;
    }
 
    public String getUserName() {
        return userName;
    }
 
    public void setUserName(String userName) {
        this.userName = userName;
    }
 
    public String getEncrytedPassword() {
        return encrytedPassword;
    }
 
    public void setEncrytedPassword(String encrytedPassword) {
        this.encrytedPassword = encrytedPassword;
    }
 
    public boolean isActive() {
        return active;
    }
 
    public void setActive(boolean active) {
        this.active = active;
    }
 
    public String getUserRole() {
        return userRole;
    }
 
    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }
 
    @Override
    public String toString() {
        return "[" + this.userName + "," + this.encrytedPassword + "," + this.userRole + "]";
    }
 
}
