package com.java.spring.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@MappedSuperclass
public class BaseEntity implements Serializable {
	private static final long serialVersionUID = 1213141516171819102L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id", nullable = false)
	private int id;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "Created_At", nullable = false)
	private Date createdAt;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="Updated_At", nullable = false)
	private Date updatedAt;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="Delete_At", nullable = false)
	private Date deletedAt;
	@Column(name = "Is_Delete", length = 1, nullable = false)
	private boolean isDeleted;
	
	public BaseEntity(int id, Date createdAt, Date updatedAt, Date deletedAt, boolean isDeleted) {
		this.id = id;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
		this.deletedAt = deletedAt;
		this.setDeleted(isDeleted);
		
	}

	public int getId() {
		return id;
	}

	public void setId(int idString) {
		this.id = idString;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getDeletedAt() {
		return deletedAt;
	}

	public void setDeletedAt(Date deletedAt) {
		this.deletedAt = deletedAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public BaseEntity() {}
}