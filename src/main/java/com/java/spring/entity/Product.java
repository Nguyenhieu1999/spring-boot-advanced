package com.java.spring.entity;

import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "Products")
@AttributeOverride(name = "id", column = @Column(name ="id", nullable = false, columnDefinition = "INT", unique = true))
public class Product extends BaseEntity {

private static final long serialVersionUID = -1000119078147252957L;

   @Id
   @Column(name = "Code", length = 20, nullable = false)
   private String code;

   @Column(name = "Name", length = 255, nullable = false)
   private String name;

   @Column(name = "Price", nullable = false)
   private double price;

   @Lob
   @Column(name = "Image", length = Integer.MAX_VALUE, nullable = true)
   private byte[] image;
    
   @Temporal(TemporalType.TIMESTAMP)
   @Column(name = "Create_Date", nullable = false)
   private Date createDate;
   public Product() {
	   super();
   }
   
   public Product(int id, String code, byte[] image, String name, double price, Date createdAt, Date deletedAt, Date updatedAt, boolean isDeleted) {
		super(id, createdAt, deletedAt, updatedAt, isDeleted);
		this.code = code;
		this.image = image;
		this.price = price;
		this.name = name;
   }
   public String getCode() {
       return code;
   }

   public void setCode(String code) {
       this.code = code;
   }

   public String getName() {
       return name;
   }

   public void setName(String name) {
       this.name = name;
   }

   public double getPrice() {
       return price;
   }

   public void setPrice(double price) {
       this.price = price;
   }

   public Date getCreateDate() {
       return createDate;
   }

   public void setCreateDate(Date createDate) {
       this.createDate = createDate;
   }

   public byte[] getImage() {
       return image;
   }

   public void setImage(byte[] image) {
       this.image = image;
   }
   public static long getSerialversionUID() {
	   return serialVersionUID;
   }

}