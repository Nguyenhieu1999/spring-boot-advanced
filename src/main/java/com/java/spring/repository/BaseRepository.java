package com.java.spring.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties.Pageable;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.query.Param;

import com.java.spring.entity.BaseEntity;
@NoRepositoryBean
interface BaseRepository <T extends BaseEntity, ID extends Serializable> extends JpaRepository<T, ID>{
	Page<T> findAll(Pageable pageable);
	@Query(value = "SELECT t.* FROM TABLE_NAME t where t.id = :id", nativeQuery= true)
	List<T> findById(@Param("id") int id);
}
