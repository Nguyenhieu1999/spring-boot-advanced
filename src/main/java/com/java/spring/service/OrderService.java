package com.java.spring.service;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.java.spring.entity.Order;
import com.java.spring.entity.OrderDetail;
import com.java.spring.entity.Product;
import com.java.spring.model.CartInfo;
import com.java.spring.model.CartLineInfo;
import com.java.spring.model.CustomerInfo;
import com.java.spring.model.OrderDetailInfo;
import com.java.spring.model.OrderInfo;
import com.java.spring.pagination.PaginationResult;
import com.java.spring.repository.OrderRepository;

@Service
public class OrderService {
	
	@Autowired
	EntityManager em;

	@Autowired
	OrderRepository orderRepository;
	
	@Autowired
	ProductService productService;
	
	@Autowired
	OrderDetailService orderDetailService;
	
	public int getMaxOrderNum() {

		int order_num = -1;
		List<Order> list = orderRepository.findAll();
		if (list.size() != 0) {
			order_num = list.get(list.size() - 1).getOrderNum();
		}

		return order_num;
	}
	
	public int getMaxId() {

		int order_num = -1;
		List<Order> list = orderRepository.findAll();
		if (list.size() != 0) {
			order_num = list.get(list.size() - 1).getId();
		}

		return order_num;
	}

	public void save(Order order) {
		orderRepository.save(order);
	}

	public Order delete(int id) {

		Order order = orderRepository.findById(id).get(0);
		order.setDeleted(true);
		order.setDeletedAt(new Date());

		this.save(order);

		return order;
	}

	public Order findById(int id) {

		List<Order> list = orderRepository.findById(id);

		if (list.size() == 0)
			return null;
		else
			return list.get(0);

	}

	public List<Order> findAll() {
		return orderRepository.findAll();
	}
	
	@Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
	public void saveOrder(CartInfo cartInfo) {

		int orderNum = this.getMaxOrderNum() + 1;
		Order order = new Order(orderNum, null, null, null, false, orderNum, null, null, null, null, null, orderNum);

		order.setId(this.getMaxId() + 1);
		order.setOrderNum(orderNum);
		order.setCreatedAt(new Date());
		order.setOrderDate(new Date());
		order.setAmount(cartInfo.getAmountTotal());

		CustomerInfo customerInfo = cartInfo.getCustomerInfo();
		order.setCustomerName(customerInfo.getName());
		order.setCustomerEmail(customerInfo.getEmail());
		order.setCustomerPhone(customerInfo.getPhone());
		order.setCustomerAddress(customerInfo.getAddress());
		
		this.save(order);
		
		List<CartLineInfo> lines = cartInfo.getCartLines();
		
		int maxIdDetail = orderDetailService.getMaxId() + 1;
		for (CartLineInfo line : lines) {
			OrderDetail detail = new OrderDetail(maxIdDetail, null, null, null, false, maxIdDetail, maxIdDetail, maxIdDetail, order, null);
			detail.setId(maxIdDetail);
			detail.setCreatedAt(new Date());
			detail.setOrder(order);
			detail.setAmount(line.getAmount());
			detail.setPrice(line.getProductInfo().getPrice());
			detail.setQuantity(line.getQuantity());
			detail.setDeleted(false);

			String code = line.getProductInfo().getCode();
			Product product = this.productService.findByCode(code);
			detail.setProduct(product);
			
			orderDetailService.save(detail);
			maxIdDetail += 1;
		}
		
		cartInfo.setOrderNum(orderNum);

	}

	public PaginationResult<OrderInfo> listOrderInfo(int page, int maxResult, int maxNavigationPage) {
		
		String sql = "Select new " + OrderInfo.class.getName()
                + "(ord.id, ord.orderDate, ord.orderNum, ord.amount, "
                + " ord.customerName, ord.customerAddress, ord.customerEmail, ord.customerPhone) " + " from "
                + Order.class.getName() + " ord "
                + " order by ord.orderNum desc";
		
		TypedQuery<OrderInfo> query = em.createQuery(sql, OrderInfo.class);
		
		return new PaginationResult<OrderInfo>(query, page, maxResult, maxNavigationPage);

	}

	public OrderInfo getOrderInfo(int orderId) {
		Order order = this.findById(orderId);
        if (order == null) {
            return null;
        }
        return new OrderInfo();
	}

	public List<OrderDetailInfo> listOrderDetailInfos(int orderId) {
		
		String sql = "Select new " + OrderDetailInfo.class.getName() 
                + "(d.id, d.product.code, d.product.name , d.quantity,d.price,d.amount) "
                + " from " + OrderDetail.class.getName() + " d "
                + " where d.order.id = " + orderId;
		
		TypedQuery<OrderDetailInfo> query = em.createQuery(sql, OrderDetailInfo.class);
		
		return query.getResultList();
	}
}


