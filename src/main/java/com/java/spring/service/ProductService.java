package com.java.spring.service;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.java.spring.entity.Product;
import com.java.spring.form.ProductForm;
import com.java.spring.model.ProductInfo;
import com.java.spring.pagination.PaginationResult;
import com.java.spring.repository.ProductRepository;

@Service
public class ProductService {
	
	@Autowired
	EntityManager em;

	@Autowired
	ProductRepository productRepository;
	
	public void save(Product product) {
		productRepository.save(product);
	}
	
	public int getMaxId() {
		
		int maxId = -1;
		
		List<Product> list = productRepository.findAll();
		
		if(list.size() > 0) maxId = list.get(list.size() - 1).getId();
		
		return maxId;
	}
	
	public Product findById(int id) {
		List<Product> list = productRepository.findById(id);
		if(list.size() == 0) return null;
		else return list.get(0);
	}
	
	public Product findByCode(String code) {
		List<Product> list = productRepository.findById(code);
		if(list.size() == 0) return null;
		else return list.get(0);
	}
	
	public List<Product> findAll(){
		return productRepository.findAll();
	}
	
	public Product delete(int id) {
		
		Product product = productRepository.findById(id).get(0);
		product.setDeleted(true);
		product.setDeletedAt(new Date());
		productRepository.save(product);
		
		return product;
	}
	
	public PaginationResult<ProductInfo> queryProduct(int page, int maxResult, int maxNavigationPage, String likeName){
		
		String sql = "Select new " + ProductInfo.class.getName() //
                + "(p.code, p.name, p.price) " + " from "//
                + Product.class.getName() + " p " + " ORDER BY p.createdAt DESC";
		TypedQuery<ProductInfo> query = em.createQuery(sql, ProductInfo.class);
		
		
		if (likeName != null && likeName.length() > 0) {
            query.setParameter("likeName", "%" + likeName.toLowerCase() + "%");
        }
		
		return  new PaginationResult<ProductInfo>(query, page, maxResult, maxNavigationPage);
	}
	
	public PaginationResult<ProductInfo> queryProducts(int page, int maxResult, int maxNavigationPage) {
        return queryProduct(page, maxResult, maxNavigationPage, null);
    }
	
	public Product saveByForm(ProductForm productForm) {
		
		String code = productForm.getCode();
		
		Product product = this.findByCode(code);
		
		if(product == null) {
			product = new Product();
			product.setId(this.getMaxId() + 1);
			product.setCreatedAt(new Date());
		}
		
		product.setCode(code);
		try {
			product.setImage(productForm.getFileData().getBytes());
		}
		catch(IOException e) {
			product.setImage((byte[]) null);
		}
		
		product.setName(productForm.getName());
		product.setPrice(productForm.getPrice());
		product.setDeleted(false);
		
		productRepository.save(product);
		
		return product;
	}
}

